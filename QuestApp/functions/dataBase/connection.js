admin = require("firebase-admin");
const serviceAccount = require("../ServiceAccountKey.json");
//const firebase = require("firebase");
//const firebaseConfig = require("../firebase.json");
//const firebaseTestConfig = require("../firebaseTest.json")

var db;

function connectDatabase() {
  //admin.initializeApp(firebaseConfig);
  admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
  });

  if (!db) {
    db = admin.firestore();
  }
  return db;
}

module.exports = connectDatabase();
