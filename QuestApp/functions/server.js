const functions = require("firebase-functions");
const admin = require("firebase-admin");
//admin.initializeApp();
const cookieParser = require("cookie-parser")();
const express = require("express");
const app = express();

var db = require("./dataBase/connection");
// CORS Express middleware to enable CORS Requests.
const cors = require("cors")({ origin: true });

// Express middleware that validates Firebase ID Tokens passed in the Authorization HTTP header.
// The Firebase ID token needs to be passed as a Bearer token in the Authorization HTTP header like this:
// `Authorization: Bearer <Firebase ID Token>`.
// when decoded successfully, the ID Token content will be added as `req.user`.
const validateFirebaseIdToken = async (req, res, next) => {
  //console.log("Check if request is authorized with Firebase ID token");

  if (
    (!req.headers.authorization ||
      !req.headers.authorization.startsWith("Bearer ")) &&
    !(req.cookies && req.cookies.__session)
  ) {
    console.error(
      "No Firebase ID token was passed as a Bearer token in the Authorization header.",
      "Make sure you authorize your request by providing the following HTTP header:",
      "Authorization: Bearer <Firebase ID Token>",
      'or by passing a "__session" cookie.'
    );
    res.status(403).send("Unauthorized");
    return;
  }

  let idToken;
  if (
    req.headers.authorization &&
    req.headers.authorization.startsWith("Bearer ")
  ) {
    //console.log('Found "Authorization" header');
    // Read the ID Token from the Authorization header.
    idToken = req.headers.authorization.split("Bearer ")[1];
  } else if (req.cookies) {
    console.log('Found "__session" cookie');
    // Read the ID Token from cookie.
    idToken = req.cookies.__session;
  } else {
    // No cookie
    res.status(403).send("Unauthorized");
    return;
  }

  try {
    const decodedIdToken = await admin.auth().verifyIdToken(idToken);
    //console.log("ID Token correctly decoded", decodedIdToken);
    req.user = decodedIdToken;
    next();
    return;
  } catch (error) {
    console.error("Error while verifying Firebase ID token:", error);
    res.status(666).send("Unauthorized");
    return;
  }
};

app.use(cors);
app.use(cookieParser);
app.use(validateFirebaseIdToken);

///////////////////////////Afegir un questionari a la BD////////////////////////////
app.post("/addForm", (req, res) => {
  console.log("Afegint qüestionari");
  res.set("Cache-Control");
  async function insertData() {
    //Introduim el formulari
    let formId = await insertFormData();
    //Introduim les preguntes
    let questionData = [];
    let questionsId = [];
    req.body.questions.map(function (question) {
      questionData.push(insertQuestionData(formId, question));
      return null;
    });
    await Promise.all(questionData)
      .then((response) => {
        questionsId = response;
        return null;
      })
      .catch(function (error) {
        console.error(error);
        return null;
      });
    //Introduim les respostes
    let responsesData = [];
    //req.body.responses.map(function () {});
    questionsId.map(function (id, index) {
      req.body.responses[index].map(function (response) {
        responsesData.push(insertResponseData(id, response));
        return null;
      });
      return null;
    });
    await Promise.all(responsesData)
      .then((response) => {
        questionsId = response;
        return null;
      })
      .catch(function (error) {
        console.error(error);
        return null;
      });
    res.status(200).send("Questionnarie saved");
  }

  function insertFormData() {
    return new Promise(function (resolve, reject) {
      try {
        db.collection("Questionnaries")
          .add({
            Active: req.body.questionnarie.active,
            Participate: req.body.questionnarie.participate,
            Participating: req.body.questionnarie.participating,
            UserId: req.body.questionnarie.userId,
            QuestionnarieName: req.body.questionnarie.questionnarieName,
            Key: req.body.questionnarie.key,
          })
          .then(function (docRef) {
            resolve(docRef.id);
            console.log("Questionnarie created");
            return null;
          })
          .catch(function (error) {
            reject(new Error(error));
          });
      } catch (msg) {
        reject(new Error(msg));
      }
    });
  }

  function insertQuestionData(formId, question) {
    return new Promise(function (resolve, reject) {
      try {
        db.collection("Questions")
          .add({
            QuestionnarieId: formId,
            Order: question.number,
            Question: question.questionText,
          })
          .then(function (docRef) {
            resolve(docRef.id);
            console.log("Question created");
            return null;
          })
          .catch(function (error) {
            reject(new Error(error));
          });
      } catch (msg) {
        reject(new Error(msg));
      }
    });
  }

  function insertResponseData(questionId, response) {
    return new Promise(function (resolve, reject) {
      try {
        db.collection("Responses")
          .add({
            QuestionId: questionId,
            Order: response.number,
            Response: response.responseText,
          })
          .then(function (docRef) {
            resolve(docRef.id);
            console.log("Response created");
            return null;
          })
          .catch(function (error) {
            reject(new Error("fail"));
          });
      } catch (msg) {
        reject(new Error("fail"));
      }
    });
  }

  try {
    insertData();
  } catch (error) {
    res.status(400).send(error);
  }
});

///////////////////////////Participar en questionari////////////////////////////
app.post("/participate", (req, res) => {
  console.log("Afegint participant");
  console.log(req.body);
  let participantNameFounded = false;
  //Comprovar si existeix el codi del questionari
  async function addParticipant() {
    let formFounded = await findQuestionnarie();
    //Comprovar si el nom d'usuari ja existeix
    if (formFounded) {
      participantNameFounded = await participantNameAlreadyExist(
        req.body.name,
        req.body.code
      );
    }
    // Si existeix el qüestionari i no existeix el participant, afegim el participant
    if (formFounded && participantNameFounded) {
      db.collection("Participants")
        .add({
          QuestionnarieId: req.body.code,
          ParticipantName: req.body.name,
        })
        .then(function (docRef) {
          let participant = {
            participantId: docRef.id,
            questionnarieId: req.body.code,
          };
          res.status(200).send(participant);
          return null;
        })
        .catch(function (error) {
          console.log(error);
        });
    }
  }

  function findQuestionnarie() {
    return new Promise(function (resolve, reject) {
      try {
        let founded;
        db.collection("Questionnaries")
          .where("Participate", "==", 1)
          .get()
          .then(function (form) {
            form.forEach((doc) => {
              if (doc.id === req.body.code) {
                resolve(true);
                console.log("formulari founded");
                founded = true;
              }
            });
            if (!founded) {
              res
                .status(601)
                .send("Aquest qüestionari no existeix o no si pot participar");
              resolve(false);
            }
            return null;
          })
          .catch(function (error) {
            console.log(error);
            reject(new Error(error));
          });
      } catch (error) {
        console.log(error);
        reject(new Error(error));
      }
    });
  }

  function participantNameAlreadyExist(name, code) {
    return new Promise(function (resolve, reject) {
      let participantNameFounded = false;
      try {
        db.collection("Participants")
          .where("ParticipantName", "==", name)
          .where("QuestionnarieId", "==", code)
          .get()
          .then(function (userName) {
            userName.forEach((doc) => {
              console.log("participant founded");
              participantNameFounded = true;
            });
            if (participantNameFounded) {
              res.status(602).send("Aquest nom ja existeix");
              resolve(false);
            } else {
              resolve(true);
            }
            return null;
          })
          .catch(function (error) {
            console.log(error);
            reject(new Error(error));
          });
      } catch (error) {
        console.log(error);
        reject(new Error(error));
      }
    });
  }

  try {
    addParticipant();
  } catch (error) {
    res.status(400).send(error);
  }
});

///////////////////////////Obtenir questionaris actius////////////////////////////
app.get("/getquestionnaries", (req, res) => {
  console.log("getting questionnaries");
  let activeQuestionnaries = [];
  res.set("Cache-Control");
  async function getActiveQuestionnariesAndSetToDefault() {
    activeQuestionnaries = await getActivesQuestionnaries(req.query.userId);

    //posem tots els questionaris actius amb configuració per defecte
    for (let aux in activeQuestionnaries) {
      db.collection("Questionnaries")
        .doc(activeQuestionnaries[aux].QuestionnarieId)
        .update({ Participate: 0, Participating: 0 });
    }
  }

  function getActivesQuestionnaries(userId) {
    return new Promise(function (resolve, reject) {
      try {
        let resolveActiveQuestionnaries = [];
        let data;
        db.collection("Questionnaries")
          .where("UserId", "==", userId)
          .get()
          .then(function (questionnarie) {
            questionnarie.forEach((doc) => {
              if (doc.data().Active === 1) {
                data = doc.data();
                data["QuestionnarieId"] = doc.id;
                resolveActiveQuestionnaries.push(data);
              }
            });
            res.status(200).send(resolveActiveQuestionnaries);
            resolve(resolveActiveQuestionnaries);
            return null;
          })
          .catch(function (error) {
            reject(new Error(error));
          });
      } catch (msg) {
        reject(new Error(msg));
      }
    });
  }

  try {
    getActiveQuestionnariesAndSetToDefault();
  } catch (error) {
    res.status(400).send(error);
  }
});

//////////////////////////////////Borrar questionari/////////////////////////////
app.delete("/deleteQuestionnarie", (req, res) => {
  console.log("Borrant qüestionari");
  let idQuestions = [];
  let idResponsesAux = [];
  let idResponses = [];
  async function getQuestionnarieQuestionsAndResponses() {
    //Agafem tots els id que haguem de borrar
    //Comprovem si l'usuari i el questionari són correctes
    let correctUserIdAndQuestionnarieId = await checkIfUserIdIsCorrect(
      req.body.questionnarieId,
      req.body.userId
    );
    if (correctUserIdAndQuestionnarieId) {
      //Obtenim els id de les preguntes
      idQuestions = await getQuestionsId(req.body.questionnarieId);
      //Obtenim els id de les respostes
      idQuestions.map(function (question) {
        idResponsesAux.push(getResponsesId(question));
        return null;
      });
      await Promise.all(idResponsesAux)
        .then((response) => {
          response.map(function (responseGroup) {
            responseGroup.map(function (response) {
              idResponses.push(response);
            });
            return null;
          });
          return null;
        })
        .catch(function (error) {
          console.error(error);
          return null;
        });
      //Borrem tots els ids
      //Borrem el questionari
      db.collection("Questionnaries")
        .doc(req.body.questionnarieId)
        .delete()
        .then(function () {
          console.log("Questionnarie successfully deleted!");
          return null;
        })
        .catch(function (error) {
          console.error("Error removing document: ", error);
          return null;
        });
      //Borrem les preguntes
      idQuestions.map(function (id) {
        db.collection("Questions")
          .doc(id)
          .delete()
          .then(function () {
            console.log("Question successfully deleted!");
            return null;
          })
          .catch(function (error) {
            console.error("Error removing document: ", error);
            return null;
          });
      });
      //Borrem les respostes
      idResponses.map(function (id) {
        db.collection("Responses")
          .doc(id)
          .delete()
          .then(function () {
            console.log("Response successfully deleted!");
            return null;
          })
          .catch(function (error) {
            console.error("Error removing document: ", error);
            return null;
          });
      });
      res.status(200).send(req.body.questionnarieId);
    }
    res.status(403).send("delete error");
  }

  function checkIfUserIdIsCorrect(questionnarieId, userId) {
    return new Promise(function (resolve, reject) {
      try {
        db.collection("Questionnaries")
          .where("UserId", "==", userId)
          .get()
          .then(function (doc) {
            doc.forEach((questionari) => {
              if (questionari.id === questionnarieId) {
                resolve(true);
              }
            });
            return null;
          })
          .catch(function (error) {
            reject(new Error(error));
          });
      } catch (msg) {
        reject(new Error(msg));
      }
    });
  }

  function getQuestionsId(questionnarieId) {
    let ids = [];
    return new Promise(function (resolve, reject) {
      try {
        console.log(questionnarieId);
        db.collection("Questions")
          .where("QuestionnarieId", "==", questionnarieId)
          .get()
          .then(function (doc) {
            doc.forEach((question) => {
              ids.push(question.id);
            });
            resolve(ids);
            return null;
          })
          .catch(function (error) {
            reject(new Error(error));
          });
      } catch (msg) {
        reject(new Error(msg));
      }
    });
  }

  function getResponsesId(questionId) {
    let ids = [];
    return new Promise(function (resolve, reject) {
      try {
        db.collection("Responses")
          .where("QuestionId", "==", questionId)
          .get()
          .then(function (doc) {
            doc.forEach((response) => {
              ids.push(response.id);
            });
            resolve(ids);
            return null;
          })
          .catch(function (error) {
            reject(new Error(error));
          });
      } catch (msg) {
        reject(new Error(msg));
      }
    });
  }

  try {
    getQuestionnarieQuestionsAndResponses();
  } catch (error) {
    res.status(400).send(error);
  }
});

///////////////////Activar participar en un qüestionari/////////////////
app.get("/setParticipateOn", (req, res) => {
  console.log("setting participate on");
  res.set("Cache-Control");
  db.collection("Questionnaries")
    .doc(req.query.questionnarieId)
    .update({ Participate: 1 });
  res.status(200).send("document updated");
});

///////////////////Desactivar participar en un qüestionari/////////////////
app.get("/setParticipateOff", (req, res) => {
  console.log("setting participate off");
  res.set("Cache-Control");
  db.collection("Questionnaries")
    .doc(req.query.questionnarieId)
    .update({ Participate: 0 });
  res.status(200).send("document updated");
});

///////////////////Activar participant en un qüestionari/////////////////
app.get("/setParticipatingOn", (req, res) => {
  console.log("setting participate on");
  res.set("Cache-Control");
  db.collection("Questionnaries")
    .doc(req.query.questionnarieId)
    .update({ Participating: 1 });
  res.status(200).send("document updated");
});

///////////////////Desactivar participant en un qüestionari/////////////////
app.get("/setParticipatingOff", (req, res) => {
  console.log("setting participate off");
  res.set("Cache-Control");
  db.collection("Questionnaries")
    .doc(req.query.questionnarieId)
    .update({ Participating: 0 });
  res.status(200).send("document updated");
});

//////////////////GET preguntes i respostes d'un questionari////////////////
app.get("/getselectedquestionnariedata", (req, res) => {
  console.log("getting questionarie data");
  res.set("Cache-Control");
  async function getQuestionsAndResponses() {
    let dataQuestions = [];
    let dataResponsesAux = [];
    let dataResponses = [];
    //Obtenim els info de les preguntes
    dataQuestions = await getQuestionsData(req.query.questionnarieId);
    //Obtenim els info de les respostes
    dataQuestions.map(function (question) {
      dataResponsesAux.push(getResponsesData(question.QuestionId));
    });
    await Promise.all(dataResponsesAux)
      .then((response) => {
        response.map(function (responseGroup) {
          responseGroup.map(function (response) {
            dataResponses.push(response);
          });
        });
        return null;
      })
      .catch(function (error) {
        console.error(error);
      });
    let data = { questionsData: dataQuestions, responsesData: dataResponses };
    console.log(data);
    res.status(200).send(data);
  }

  function getQuestionsData(questionnarieId) {
    let questionData = [];
    return new Promise(function (resolve, reject) {
      try {
        console.log(questionnarieId);
        db.collection("Questions")
          .where("QuestionnarieId", "==", questionnarieId)
          .get()
          .then(function (doc) {
            let data = [];
            doc.forEach((question) => {
              data = question.data();
              data["QuestionId"] = question.id;
              questionData.push(data);
            });
            resolve(questionData);
            return null;
          })
          .catch(function (error) {
            reject(new Error(error));
          });
      } catch (msg) {
        reject(new Error(msg));
      }
    });
  }

  function getResponsesData(questionId) {
    let responseData = [];
    return new Promise(function (resolve, reject) {
      try {
        db.collection("Responses")
          .where("QuestionId", "==", questionId)
          .get()
          .then(function (doc) {
            let data = [];
            doc.forEach((response) => {
              data = response.data();
              data["ResponseId"] = response.id;
              responseData.push(data);
            });
            resolve(responseData);
            return null;
          })
          .catch(function (error) {
            reject(new Error(error));
          });
      } catch (msg) {
        reject(new Error(msg));
      }
    });
  }

  try {
    getQuestionsAndResponses();
  } catch (error) {
    res.status(400).send(error);
  }
});

//////////////////Posar questionaria al historial////////////////
app.get("/setToHistory", (req, res) => {
  console.log("setting questionnarie to history");
  res.set("Cache-Control");
  db.collection("Questionnaries")
    .doc(req.query.questionnarieId)
    .update({ Participating: 0, Active: 0 });
  res.status(200).send("document updated");
});

//////////////////Posar questionaria al historial////////////////
app.post("/saveResponse", (req, res) => {
  db.collection("ResponsesParticipants")
    .add({
      ResponseId: req.body.responseId,
      ParticipantId: req.body.participantId,
    })
    .then(function (docRef) {
      res.status(200).send("resposta afegida");
      return null;
    })
    .catch(function (error) {
      console.log(error);
    });
});

/////////////////Obtenir historial////////////////////
app.get("/getHistory", (req, res) => {
  console.log("getting history");
  let history = [];
  res.set("Cache-Control");
  db.collection("Questionnaries")
    .where("UserId", "==", req.query.userId)
    .where("Active", "==", 0)
    .get()
    .then(function (questionnarie) {
      let information = [];
      questionnarie.forEach((doc) => {
        information = doc.data();
        information["QuestionnarieId"] = doc.id;
        history.push(information);
      });
      res.status(200).send(history);
      return null;
    })
    .catch(function (error) {
      console.error(error);
    });
});

/////////////////Obtenir les dades d'un qüestionari de l'historial////////////////////
app.get("/getHistoryData", (req, res) => {
  console.log("getting history data");
  console.log(req.query.questionnarieId);
  res.set("Cache-Control");
  async function getQuestionsAndResponses() {
    let dataQuestions = [];
    let dataResponsesAux = [];
    let dataResponses = [];
    let counterAux = [];
    //Obtenim la info de les preguntes
    dataQuestions = await getQuestionsData(req.query.questionnarieId);
    //Obtenim la info de les respostes
    dataQuestions.map(function (question) {
      dataResponsesAux.push(getResponsesData(question.QuestionId));
    });
    await Promise.all(dataResponsesAux)
      .then((response) => {
        response.map(function (responseGroup) {
          responseGroup.map(function (response) {
            dataResponses.push(response);
          });
        });
        return null;
      })
      .catch(function (error) {
        console.error(error);
      });
    //Contem les respostes que te cada pregunta
    dataResponses.map((response, key) => {
      counterAux.push(getNumberOfResponses(response.ResponseId));
    });
    await Promise.all(counterAux)
      .then((response) => {
        console.log("ultim");
        console.log(response);
        console.log(dataResponses);
        for (let i in dataResponses) {
          for (let j in response) {
            if (response[j].responseId === dataResponses[i].ResponseId) {
              dataResponses[i]["counter"] = response[j].counter;
            }
          }
        }
        return null;
      })
      .catch(function (error) {
        console.error(error);
      });
    let data = { questionsData: dataQuestions, responsesData: dataResponses };
    console.log(data);
    res.status(200).send(data);
  }

  function getQuestionsData(questionnarieId) {
    let questionData = [];
    return new Promise(function (resolve, reject) {
      try {
        console.log(questionnarieId);
        db.collection("Questions")
          .where("QuestionnarieId", "==", questionnarieId)
          .get()
          .then(function (doc) {
            let data = [];
            doc.forEach((question) => {
              data = question.data();
              data["QuestionId"] = question.id;
              questionData.push(data);
            });
            resolve(questionData);
            return null;
          })
          .catch(function (error) {
            reject(new Error(error));
          });
      } catch (msg) {
        reject(new Error(msg));
      }
    });
  }

  function getResponsesData(questionId) {
    let responseData = [];
    return new Promise(function (resolve, reject) {
      try {
        db.collection("Responses")
          .where("QuestionId", "==", questionId)
          .get()
          .then(function (doc) {
            let data = [];
            doc.forEach((response) => {
              data = response.data();
              data["ResponseId"] = response.id;
              responseData.push(data);
            });
            resolve(responseData);
            return null;
          })
          .catch(function (error) {
            reject(new Error(error));
          });
      } catch (msg) {
        reject(new Error(msg));
      }
    });
  }

  function getNumberOfResponses(responseId) {
    return new Promise(function (resolve, reject) {
      try {
        db.collection("ResponsesParticipants")
          .where("ResponseId", "==", responseId)
          .get()
          .then(function (doc) {
            let counter = 0;
            doc.forEach(() => {
              counter++;
            });
            console.log("Respostes: " + counter);
            let data = {
              counter: counter,
              responseId: responseId,
            };
            resolve(data);
            return null;
          })
          .catch(function (error) {
            reject(new Error(error));
          });
      } catch (msg) {
        reject(new Error(msg));
      }
    });
  }

  try {
    getQuestionsAndResponses();
  } catch (error) {
    res.status(400).send(error);
  }
});

exports.app = functions.https.onRequest(app);
