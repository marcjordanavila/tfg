import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import firebase from "./firebase";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";

import History from "./components/history/history";
import HistoryData from "./components/historyData/historyData";
import AdminParticipating from "./components/participateScreen/participating/adminParticipating/adminParticipating";
import UserParticipating from "./components/participateScreen/participating/userParticipating/userParticipating";
import LoginScreen from "./components/loginScreen/loginScreen";
import PrincipalPanelScreen from "./components/principalPanelScreen/principalPanelScreen";
import ChangePassword from "./components/loginScreen/changePassword/changePassword";
import CreateForm from "./components/createForm/createForm";
import CreateQuestion from "./components/createQuestion/createQuestion";
import Participate from "./components/participateScreen/participate/participate";
import Questionnaries from "./components/questionnaries/questionnaries";
import QuestionnarieData from "./components/questionnarieData/questionnarieData";
import {
  authenticatedUser,
  isAutenticated,
  getToken,
  getUserId,
} from "./actions/userActions";

import "./generalcss.css";
import logo from "./images/QuestApp.png";

class App extends Component {
  componentDidMount = () => {
    var that = this;
    // Mirem si estem autenticats
    firebase.auth().onAuthStateChanged((authenticated) => {
      if (authenticated) {
        this.props.isAutenticated(authenticated.isAnonymous);
        this.props.getUserId(authenticated.uid);
        // Agafem el token i el guardem
        firebase
          .auth()
          .currentUser.getIdToken(true)
          .then(function (idToken) {
            if (idToken) {
              that.props.getToken(idToken);
            }
          });
      }
      this.props.authenticatedUser(authenticated);
    });
  };

  render() {
    return (
      <div className="App col-s-12 col-12">
        <div className="divLogo">
          <img src={logo} alt="logo" className="logo" />
        </div>
        <Router>
          <Switch>
            <Redirect exact from="/" to="principalpanel" />
            <Route exact path="/login">
              {this.props.userReducer.userAuth ? (
                this.props.userReducer.isAnonymous ? (
                  <Redirect to="participate" />
                ) : (
                  <Redirect to="principalpanel" />
                )
              ) : (
                <LoginScreen />
              )}
            </Route>
            <Route exact path="/principalpanel">
              {this.props.userReducer.userAuth ? (
                <PrincipalPanelScreen />
              ) : (
                <Redirect to="login" />
              )}
            </Route>
            <Route exact path="/participate">
              {this.props.userReducer.userAuth ? (
                <Participate />
              ) : (
                <Redirect to="login" />
              )}
            </Route>
            <Route exact path="/changepassword">
              <ChangePassword />
            </Route>
            <Route exact path="/createform">
              {this.props.userReducer.userAuth &&
              !this.props.userReducer.isAnonymous ? (
                <CreateForm />
              ) : (
                <Redirect to="principalpanel" />
              )}
            </Route>
            <Route exact path="/question">
              {this.props.userReducer.userAuth &&
              !this.props.userReducer.isAnonymous ? (
                <CreateQuestion />
              ) : (
                <Redirect to="principalpanel" />
              )}
            </Route>
            <Route exact path="/questionnaires">
              {this.props.userReducer.userAuth &&
              !this.props.userReducer.isAnonymous ? (
                <Questionnaries />
              ) : (
                <Redirect to="principalpanel" />
              )}
            </Route>
            <Route exact path="/questionnairedata">
              {this.props.userReducer.userAuth &&
              !this.props.userReducer.isAnonymous ? (
                <QuestionnarieData />
              ) : (
                <Redirect to="principalpanel" />
              )}
            </Route>
            <Route exact path="/adminparticipating">
              {this.props.userReducer.userAuth &&
              !this.props.userReducer.isAnonymous ? (
                <AdminParticipating />
              ) : (
                <Redirect to="principalpanel" />
              )}
            </Route>
            <Route exact path="/userparticipating">
              {this.props.userReducer.userAuth ? (
                <UserParticipating />
              ) : (
                <Redirect to="principalpanel" />
              )}
            </Route>
            <Route exact path="/history">
              {this.props.userReducer.userAuth &&
              !this.props.userReducer.isAnonymous ? (
                <History />
              ) : (
                <Redirect to="principalpanel" />
              )}
            </Route>
            <Route exact path="/historydata">
              {this.props.userReducer.userAuth &&
              !this.props.userReducer.isAnonymous ? (
                <HistoryData />
              ) : (
                <Redirect to="principalpanel" />
              )}
            </Route>
          </Switch>
        </Router>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    generalReducer: state.generalReducer,
    userReducer: state.userReducer,
  };
};

App.propTypes = {
  generalReducer: PropTypes.object.isRequired,
  userReducer: PropTypes.object.isRequired,
  authenticatedUser: PropTypes.func.isRequired,
  isAutenticated: PropTypes.func.isRequired,
  getToken: PropTypes.func.isRequired,
  getUserId: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, {
  authenticatedUser,
  isAutenticated,
  getToken,
  getUserId,
})(App);
