import axios from "axios";

import * as actionTypes from "../actions/actions";

//test = 0 produccio || test = 1 test
let test = 0;
let functionsURL =
  test === 0
    ? "https://us-central1-appquest-dcb91.cloudfunctions.net/app"
    : "http://localhost:5001/appquest-dcb91/us-central1/app";

export const addFormName = (name) => {
  return {
    type: actionTypes.ADD_NAME_FORM,
    payload: name,
  };
};

export const addQuestionToForm = () => {
  return {
    type: actionTypes.ADD_QUESTION_FORM,
  };
};

export const addQuestionText = (data) => {
  return {
    type: actionTypes.ADD_QUESTION_TEXT,
    payload: data,
  };
};

export const addResponseToQuestion = (questionNumber) => {
  return {
    type: actionTypes.ADD_RESPONSE_TO_QUESTION,
    payload: questionNumber,
  };
};

export const addResponseText = (data) => {
  return {
    type: actionTypes.ADD_RESPONSE_TEXT_TO_QUESTION,
    payload: data,
  };
};

export const cancelCreateForm = () => {
  return {
    type: actionTypes.CANCEL_QUESTION_FORM,
  };
};

export const cancelQuestionAndResponses = () => {
  return {
    type: actionTypes.CANCEL_QUESTION_AND_RESPONSE,
  };
};

export const createForm = (questionsData, token) => (dispatch) => {
  let config = {
    headers: {
      "X-Requested-With": "XMLHttpRequest",
      "X-Powered-By": "Express",
      "Content-Type": "application/json; charset=utf-8",
      "Access-Control-Allow-Origin": "*",
      Authorization: `Bearer ${token}`,
    },
  };
  return axios
    .post(`${functionsURL}/addForm`, questionsData, config)
    .then((res) =>
      dispatch({
        type: actionTypes.CREATE_FORM,
        payload: res.data,
      })
    );
};

export const deleteQuestionAndResponses = (questionNumber) => {
  return {
    type: actionTypes.DELETE_QUESTION_AND_RESPONSES,
    payload: questionNumber,
  };
};

export const deleteQuestionnarie = (questionnarieData, token) => (dispatch) => {
  let config = {
    headers: {
      "X-Requested-With": "XMLHttpRequest",
      "X-Powered-By": "Express",
      "Content-Type": "application/json; charset=utf-8",
      "Access-Control-Allow-Origin": "*",
      Authorization: `Bearer ${token}`,
    },
    data: questionnarieData,
  };
  return axios
    .delete(`${functionsURL}/deleteQuestionnarie`, config)
    .then((res) =>
      dispatch({
        type: actionTypes.DELETE_QUESTIONNARIE,
        payload: res.data,
      })
    );
};

export const deleteResponse = (data) => {
  return {
    type: actionTypes.DELETE_RESPONSE,
    payload: data,
  };
};

export const getActiveQuestionnaries = (userId, token) => (dispatch) => {
  let config = {
    headers: {
      "X-Requested-With": "XMLHttpRequest",
      "X-Powered-By": "Express",
      "Content-Type": "application/json; charset=utf-8",
      "Access-Control-Allow-Origin": "*",
      Authorization: `Bearer ${token}`,
    },
  };
  return axios
    .get(`${functionsURL}/getquestionnaries?userId=${userId}`, config)
    .then((res) =>
      dispatch({
        type: actionTypes.GET_ACTIVE_QUESTIONARIES,
        payload: res.data,
      })
    );
};

export const getHistory = (userId, token) => (dispatch) => {
  let config = {
    headers: {
      "X-Requested-With": "XMLHttpRequest",
      "X-Powered-By": "Express",
      "Content-Type": "application/json; charset=utf-8",
      "Access-Control-Allow-Origin": "*",
      Authorization: `Bearer ${token}`,
    },
  };
  return axios
    .get(`${functionsURL}/getHistory?userId=${userId}`, config)
    .then((res) =>
      dispatch({
        type: actionTypes.GET_HISTORY,
        payload: res.data,
      })
    );
};

export const getHistoryData = (questionnarieId, token) => (dispatch) => {
  let config = {
    headers: {
      "X-Requested-With": "XMLHttpRequest",
      "X-Powered-By": "Express",
      "Content-Type": "application/json; charset=utf-8",
      "Access-Control-Allow-Origin": "*",
      Authorization: `Bearer ${token}`,
    },
  };
  return axios
    .get(
      `${functionsURL}/getHistoryData?questionnarieId=${questionnarieId}`,
      config
    )
    .then((res) =>
      dispatch({
        type: actionTypes.GET_HISTORY_DATA,
        payload: res.data,
      })
    );
};

export const getQuestionsAndResponsesSelectedQuestionnarie = (
  questionnarieId,
  token
) => (dispatch) => {
  let config = {
    headers: {
      "X-Requested-With": "XMLHttpRequest",
      "X-Powered-By": "Express",
      "Content-Type": "application/json; charset=utf-8",
      "Access-Control-Allow-Origin": "*",
      Authorization: `Bearer ${token}`,
    },
  };
  return axios
    .get(
      `${functionsURL}/getselectedquestionnariedata?questionnarieId=${questionnarieId}`,
      config
    )
    .then((res) =>
      dispatch({
        type: actionTypes.GET_QUESTIONS_AND_RESPOSNES_SELECTED_QUESTIONARIE,
        payload: res.data,
      })
    );
};

export const getParticipateQuestionnarieId = (questionnarieId) => {
  return {
    type: actionTypes.GET_PARTICIPATE_QUESTIONNARIE_ID,
    payload: questionnarieId,
  };
};

export const participate = (participateData, token) => (dispatch) => {
  let config = {
    headers: {
      "X-Requested-With": "XMLHttpRequest",
      "X-Powered-By": "Express",
      "Content-Type": "application/json; charset=utf-8",
      "Access-Control-Allow-Origin": "*",
      Authorization: `Bearer ${token}`,
    },
  };
  return axios
    .post(`${functionsURL}/participate`, participateData, config)
    .then((res) =>
      dispatch({
        type: actionTypes.PARTICIPATE,
        payload: res.data,
      })
    );
};

export const saveHistoryData = (data) => {
  return {
    type: actionTypes.SAVE_HISTORY_DATA,
    payload: data,
  };
};

export const selectedQuestionnarie = (selectedQuestionnarie) => {
  return {
    type: actionTypes.SELECTED_QUESTIONARI,
    payload: selectedQuestionnarie,
  };
};

export const selectedQuestion = (questionNumber) => {
  return {
    type: actionTypes.SELECTED_QUESTION,
    payload: questionNumber,
  };
};

export const setQuestionariParticipateOn = (questionnarieId, token) => (
  dispatch
) => {
  let config = {
    headers: {
      "X-Requested-With": "XMLHttpRequest",
      "X-Powered-By": "Express",
      "Content-Type": "application/json; charset=utf-8",
      "Access-Control-Allow-Origin": "*",
      Authorization: `Bearer ${token}`,
    },
  };
  return axios
    .get(
      `${functionsURL}/setParticipateOn?questionnarieId=${questionnarieId}`,
      config
    )
    .then((res) =>
      dispatch({
        type: actionTypes.SET_PARTICIPATE_ON,
        payload: res.data,
      })
    );
};

export const setQuestionariParticipateOff = (questionnarieId, token) => (
  dispatch
) => {
  let config = {
    headers: {
      "X-Requested-With": "XMLHttpRequest",
      "X-Powered-By": "Express",
      "Content-Type": "application/json; charset=utf-8",
      "Access-Control-Allow-Origin": "*",
      Authorization: `Bearer ${token}`,
    },
  };
  return axios
    .get(
      `${functionsURL}/setParticipateOff?questionnarieId=${questionnarieId}`,
      config
    )
    .then((res) =>
      dispatch({
        type: actionTypes.SET_PARTICIPATE_OFF,
        payload: res.data,
      })
    );
};

export const setQuestionariParticipatingOn = (questionnarieId, token) => (
  dispatch
) => {
  let config = {
    headers: {
      "X-Requested-With": "XMLHttpRequest",
      "X-Powered-By": "Express",
      "Content-Type": "application/json; charset=utf-8",
      "Access-Control-Allow-Origin": "*",
      Authorization: `Bearer ${token}`,
    },
  };
  return axios
    .get(
      `${functionsURL}/setParticipatingOn?questionnarieId=${questionnarieId}`,
      config
    )
    .then((res) =>
      dispatch({
        type: actionTypes.SET_PARTICIPATING_OFF,
        payload: res.data,
      })
    );
};

export const setQuestionariParticipatingOff = (questionnarieId, token) => (
  dispatch
) => {
  let config = {
    headers: {
      "X-Requested-With": "XMLHttpRequest",
      "X-Powered-By": "Express",
      "Content-Type": "application/json; charset=utf-8",
      "Access-Control-Allow-Origin": "*",
      Authorization: `Bearer ${token}`,
    },
  };
  return axios
    .get(
      `${functionsURL}/setParticipatingOff?questionnarieId=${questionnarieId}`,
      config
    )
    .then((res) =>
      dispatch({
        type: actionTypes.SET_PARTICIPATING_OFF,
        payload: res.data,
      })
    );
};

export const setQuestionariToHistory = (questionnarieId, token) => (
  dispatch
) => {
  let config = {
    headers: {
      "X-Requested-With": "XMLHttpRequest",
      "X-Powered-By": "Express",
      "Content-Type": "application/json; charset=utf-8",
      "Access-Control-Allow-Origin": "*",
      Authorization: `Bearer ${token}`,
    },
  };
  return axios
    .get(
      `${functionsURL}/setToHistory?questionnarieId=${questionnarieId}`,
      config
    )
    .then((res) =>
      dispatch({
        type: actionTypes.SET_TO_HISTORY,
        payload: res.data,
      })
    );
};

export const saveResponse = (responseData, token) => (dispatch) => {
  let config = {
    headers: {
      "X-Requested-With": "XMLHttpRequest",
      "X-Powered-By": "Express",
      "Content-Type": "application/json; charset=utf-8",
      "Access-Control-Allow-Origin": "*",
      Authorization: `Bearer ${token}`,
    },
  };
  return axios
    .post(`${functionsURL}/saveResponse`, responseData, config)
    .then((res) =>
      dispatch({
        type: actionTypes.SAVE_RESPONSE,
        payload: res.data,
      })
    );
};
