//import axios from "axios";

import * as actionTypes from "../actions/actions";

export const authenticatedUser = (auth) => {
  if (auth) {
    return {
      type: actionTypes.IS_LOGGED,
    };
  } else {
    return {
      type: actionTypes.IS_NOT_LOGGED,
    };
  }
};

export const getToken = (idToken) => {
  return {
    type: actionTypes.GET_TOKEN,
    payload: idToken,
  };
};

export const getUserId = (userId) => {
  return {
    type: actionTypes.GET_USER_ID,
    payload: userId,
  };
};

export const isAutenticated = (userType) => {
  return {
    type: actionTypes.IS_ANONIMOUS,
    payload: userType,
  };
};

export const loginUser = () => {
  return {
    type: actionTypes.SING_IN_USER,
  };
};

export const registerUser = () => {
  return {
    type: actionTypes.SING_UP_USER,
  };
};

export const singOut = () => {
  return {
    type: actionTypes.SING_OUT,
  };
};
