import React from "react";

import "./button.css";

export default function Button(props) {
  return (
    <div className="buttonDiv col-s-12 col-12">
      <button
        type="button"
        className={props.class ? props.class : ""}
        onClick={props.onClickButton}
        disabled={props.disabled}
      >
        {props.buttonName}
      </button>
    </div>
  );
}
