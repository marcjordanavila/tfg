import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faInfo, faTimes } from "@fortawesome/free-solid-svg-icons";

import "./inputType.css";

export default function InputType(props) {
  let element;
  if (props.icon === "info") {
    element = (
      <span title={props.information} className="spanInformationIcon">
        <FontAwesomeIcon icon={faInfo} size="xs" className="infoIcon" />
      </span>
    );
  } else {
    element = "";
  }
  return (
    <div className="inputTypeDiv">
      {props.title}
      {props.icon && element}
      <input
        className={props.class}
        type={props.type}
        name={props.name}
        value={props.value}
        onChange={props.onChangeFunction}
        placeholder={props.placeholder}
        autoComplete="false"
      />
      {props.delete && props.number >= 2 && (
        <FontAwesomeIcon
          icon={faTimes}
          className="removeItem floatRigth"
          onClick={props.onClickDeleteButton}
        />
      )}
    </div>
  );
}
