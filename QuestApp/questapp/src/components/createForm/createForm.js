import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { Link, Redirect } from "react-router-dom";
import firebase from "../../firebase";
import ClipLoader from "react-spinners/ClipLoader";

import Button from "../basicComponents/button/button";
import InputType from "../basicComponents/inputType/inputType";
import {
  addQuestionToForm,
  cancelCreateForm,
  selectedQuestion,
  createForm,
  addFormName,
} from "../../actions/generalActions";
import { singOut } from "../../actions/userActions";

import "./createForm.css";

class CreateForm extends Component {
  state = {
    name: "",
    loading: false,
    created: false,
  };

  componentDidMount = () => {
    if (
      this.state.name === "" &&
      this.state.name !== this.props.generalReducer.form.name
    ) {
      this.setState({
        name: this.props.generalReducer.form.name,
      });
    }
  };

  getQuestionsData = () => {
    let questionsObject = [];
    this.props.generalReducer.questions.map((question) => {
      questionsObject.push({
        questionText: question.question,
        number: question.questionNumber,
      });
    });
    return questionsObject;
  };

  getResponsesData = () => {
    let responsesObject = [];
    this.props.generalReducer.questions.map((question, index) => {
      responsesObject.push([]);
      question.responses.map((response) => {
        responsesObject[index].push({
          responseText: response.response,
          number: response.counter,
        });
      });
    });
    return responsesObject;
  };

  handleOnChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  handleOnClickCreate = () => {
    this.setState({
      loading: true,
    });
    let questionariData = {
      active: this.props.generalReducer.form.active,
      participate: this.props.generalReducer.form.participate,
      participating: this.props.generalReducer.form.participating,
      userId: this.props.userReducer.userId,
      questionnarieName: this.state.name,
      key: this.props.generalReducer.form.key,
    };
    let questionsData = this.getQuestionsData();
    let responsesData = this.getResponsesData();
    let dataToSend = {
      questionnarie: questionariData,
      questions: questionsData,
      responses: responsesData,
    };
    let token = this.props.userReducer.token;
    this.props
      .createForm(dataToSend, token)
      .then(() => {
        this.setState({
          loading: false,
          created: true,
        });
      })
      .catch(function (error) {
        this.setState({
          loading: false,
        });
        if (error.response?.status === 666) {
          firebase.auth().signOut();
          this.props.singOut();
        }
      });
  };

  handleOnClickQuestionButton = (key) => {
    this.props.addFormName(this.state.name);
    this.props.selectedQuestion(key);
  };

  render() {
    return (
      <div className="createFormGeneralDiv backgroundColorWhite">
        {this.state.loading && (
          <div className="externalSpinnerDiv">
            <div className="spinner">
              <ClipLoader
                className="spinner"
                size={50}
                color={"#00a0b9"}
                loading={this.state.loading}
              />
            </div>
          </div>
        )}
        <div className="col-s-12 col-12">
          <InputType
            placeholder="Nom del Qüestionari"
            name="name"
            type="text"
            value={this.state.name}
            onChangeFunction={this.handleOnChange}
          />
        </div>
        {this.props.generalReducer.questionButtons.map((button, key) => {
          return (
            <div className="col-s-12 col-12" key={key}>
              <Link to={"question"}>
                <Button
                  buttonName={"Pregunta " + (key + 1)}
                  onClickButton={() => this.handleOnClickQuestionButton(key)}
                  class="buttonStyle"
                />
              </Link>
            </div>
          );
        })}
        <div className="col-s-12 col-12">
          {this.props.generalReducer.questionButtons.length < 5 && (
            <Button
              buttonName="Afegir pregunta"
              onClickButton={this.props.addQuestionToForm}
              class="buttonStyle"
            />
          )}
        </div>
        <div className="col-s-12 col-12 cancelButtonCreateForm">
          <Link to="principalpanel">
            <Button
              buttonName="Cancelar"
              class="buttonStyle"
              onClickButton={this.props.cancelCreateForm}
            />
          </Link>
        </div>
        {
          // Perque es pugui veure el spinner
          this.state.created && <Redirect to="principalpanel" />
        }
        <div className="col-s-12 col-12">
          <Button
            buttonName="Crear"
            class="buttonStyle"
            onClickButton={this.handleOnClickCreate}
          />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    generalReducer: state.generalReducer,
    userReducer: state.userReducer,
  };
};

CreateForm.propTypes = {
  generalReducer: PropTypes.object.isRequired,
  userReducer: PropTypes.object.isRequired,
  addFormName: PropTypes.func.isRequired,
  addQuestionToForm: PropTypes.func.isRequired,
  cancelCreateForm: PropTypes.func.isRequired,
  createForm: PropTypes.func.isRequired,
  selectedQuestion: PropTypes.func.isRequired,
  singOut: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, {
  addFormName,
  addQuestionToForm,
  cancelCreateForm,
  createForm,
  selectedQuestion,
  singOut,
})(CreateForm);
