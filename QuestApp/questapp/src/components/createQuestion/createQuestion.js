import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

import InputType from "../basicComponents/inputType/inputType";
import Button from "../basicComponents/button/button";
import {
  addQuestionText,
  cancelQuestionAndResponses,
  addResponseToQuestion,
  addResponseText,
  deleteResponse,
  deleteQuestionAndResponses,
} from "../../actions/generalActions";

import "./createQuestion.css";

class CreateQuestion extends Component {
  state = {
    questionNumber: "",
    question: "",
    response1: "",
    response2: "",
    response3: "",
    response4: "",
    response5: "",
  };

  componentDidMount = () => {
    //Carragem els valors del generalReducer
    if (
      this.props.generalReducer.selectedQuestion !== this.state.questionNumber
    ) {
      this.setState({
        questionNumber: this.props.generalReducer.selectedQuestion,
        question: this.props.generalReducer.questions[
          this.props.generalReducer.selectedQuestion
        ].question,
        response1: this.props.generalReducer.questions[
          this.props.generalReducer.selectedQuestion
        ].responses[0].response,
        response2: this.props.generalReducer.questions[
          this.props.generalReducer.selectedQuestion
        ].responses[1].response,
        response3: this.props.generalReducer.questions[
          this.props.generalReducer.selectedQuestion
        ].responses[2]
          ? this.props.generalReducer.questions[
              this.props.generalReducer.selectedQuestion
            ].responses[2].response
          : "",
        response4: this.props.generalReducer.questions[
          this.props.generalReducer.selectedQuestion
        ].responses[3]
          ? this.props.generalReducer.questions[
              this.props.generalReducer.selectedQuestion
            ].responses[3].response
          : "",
        response5: this.props.generalReducer.questions[
          this.props.generalReducer.selectedQuestion
        ].responses[4]
          ? this.props.generalReducer.questions[
              this.props.generalReducer.selectedQuestion
            ].responses[4].response
          : "",
      });
    }
  };

  handleOnChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  confirmQuestionAndResponses = () => {
    var question = {
      question: this.state.question,
      questionNumber: this.state.questionNumber,
    };
    var dataQuestion = {
      responses: [
        this.state.response1,
        this.state.response2,
        this.state.response3,
        this.state.response4,
        this.state.response5,
      ],
      questionNumber: this.state.questionNumber,
    };
    this.props.addQuestionText(question);
    this.props.addResponseText(dataQuestion);
  };

  handleClickDeleteQuestionAndResponses = () => {
    this.props.deleteQuestionAndResponses(this.state.questionNumber);
  };

  handleClickDeleteResponse = (index) => {
    console.log(index);
    var data = {
      index: index,
      questionNumber: this.state.questionNumber,
    };

    if (index === 2) {
      this.setState({
        response3: this.state.response4,
        response4: this.state.response5,
        response5: "",
      });
    } else if (index === 3) {
      this.setState({
        response4: this.state.response5,
        response5: "",
      });
    } else if (index === 4) {
      this.setState({
        response5: "",
      });
    }

    this.props.deleteResponse(data);
  };

  getResponseValue = (index) => {
    if (index === 1) return this.state.response1;
    if (index === 2) return this.state.response2;
    if (index === 3) return this.state.response3;
    if (index === 4) return this.state.response4;
    if (index === 5) return this.state.response5;
    return "";
  };

  render() {
    return (
      <div className="createQuestionGeneralDiv backgroundColorWhite">
        <div className="col-s-12 col-12">
          <InputType
            placeholder="Pregunta"
            name="question"
            type="text"
            value={this.state.question}
            onChangeFunction={this.handleOnChange}
          />
        </div>
        {this.props.generalReducer.questions[
          this.props.generalReducer.selectedQuestion
        ].responses.map((response, key) => {
          return (
            <div key={key}>
              <InputType
                placeholder={"Resposta " + (key + 1)}
                name={"response" + (key + 1)}
                value={this.getResponseValue(key + 1)}
                type="text"
                class={key >= 2 ? "inputWithRemove" : ""}
                onChangeFunction={this.handleOnChange}
                delete={true}
                number={key}
                onClickDeleteButton={() => this.handleClickDeleteResponse(key)}
              />
            </div>
          );
        })}
        <div className="col-s-12 col-12">
          {this.props.generalReducer.questions[
            this.props.generalReducer.selectedQuestion
          ].responses.length < 5 && (
            <Button
              buttonName="Afegir resposta"
              onClickButton={() =>
                this.props.addResponseToQuestion(this.state.questionNumber)
              }
              class="buttonStyle"
            />
          )}
        </div>
        <div className="col-s-12 col-12">
          <Link to="createform">
            <Button
              buttonName="Confirmar"
              onClickButton={this.confirmQuestionAndResponses}
              class="buttonStyle"
            />
          </Link>
        </div>
        <div className="col-s-12 col-12 marginTopCreateQuestion">
          <Link to="createform">
            <Button
              buttonName="Cancelar"
              onClickButton={this.props.cancelQuestionAndResponses}
              class="buttonStyle"
            />
          </Link>
        </div>
        <div className="col-s-12 col-12">
          <Link to="createform">
            <Button
              buttonName="Eliminar"
              onClickButton={this.handleClickDeleteQuestionAndResponses}
              class="buttonStyle"
              disabled={this.state.questionNumber === 0 ? true : false}
            />
          </Link>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    generalReducer: state.generalReducer,
  };
};

CreateQuestion.propTypes = {
  generalReducer: PropTypes.object.isRequired,
  addQuestionText: PropTypes.func.isRequired,
  cancelQuestionAndResponses: PropTypes.func.isRequired,
  addResponseToQuestion: PropTypes.func.isRequired,
  addResponseText: PropTypes.func.isRequired,
  deleteResponse: PropTypes.func.isRequired,
  deleteQuestionAndResponses: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, {
  addQuestionText,
  cancelQuestionAndResponses,
  addResponseToQuestion,
  addResponseText,
  deleteResponse,
  deleteQuestionAndResponses,
})(CreateQuestion);
