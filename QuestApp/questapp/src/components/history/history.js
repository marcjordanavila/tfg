import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import firebase from "../../firebase";
import { Link } from "react-router-dom";
import ClipLoader from "react-spinners/ClipLoader";

import { getHistory, saveHistoryData } from "../../actions/generalActions";
import Button from "../basicComponents/button/button";

import "./history.css";

class History extends Component {
  state = {
    loading: false,
  };

  componentDidMount = () => {
    this.setState({
      loading: true,
    });
    let userId = this.props.userReducer.userId;
    let token = this.props.userReducer.token;
    this.props
      .getHistory(userId, token)
      .then(() => {
        this.setState({
          loading: false,
        });
      })
      .catch((error) => {
        this.setState({
          loading: false,
        });
        if (error.response.status === 666) {
          firebase.auth().signOut();
          this.props.singOut();
        }
      });
  };

  handleOnClickHistoryButton = (key) => {
    let data = {
      questionnarieId: this.props.generalReducer.historyQuestions[key]
        .QuestionnarieId,
      questionnarieTitle: this.props.generalReducer.historyQuestions[key]
        .QuestionnarieName,
    };
    this.props.saveHistoryData(data);
  };

  render() {
    return (
      <div className="generalDiv backgroundColorWhite">
        {this.state.loading && (
          <div className="externalSpinnerDiv">
            <div className="spinner">
              <ClipLoader
                className="spinner"
                size={50}
                color={"#00a0b9"}
                loading={this.state.loading}
              />
            </div>
          </div>
        )}
        <div className="col-s-12 col-12 centerText historyScreenTitle">
          <h1>Historial</h1>
        </div>
        {this.props.generalReducer.historyQuestions.length === 0 ? (
          <div className="col-s-12 col-12 centerText">
            <p>No té cap qüestionari en l'historial</p>
          </div>
        ) : (
          this.props.generalReducer.historyQuestions.map(
            (questionnarie, key) => {
              return (
                <div className="col-s-12 col-12" key={key}>
                  <Link to="historydata">
                    <Button
                      buttonName={questionnarie.QuestionnarieName}
                      onClickButton={() => this.handleOnClickHistoryButton(key)}
                      class="buttonStyle "
                    />
                  </Link>
                </div>
              );
            }
          )
        )}
        <div className="col-s-12 col-12 cancelButtonHistory">
          <Link to="principalpanel">
            <Button buttonName="Cancelar" class="buttonStyle" />
          </Link>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    generalReducer: state.generalReducer,
    userReducer: state.userReducer,
  };
};

History.propTypes = {
  generalReducer: PropTypes.object.isRequired,
  userReducer: PropTypes.object.isRequired,
  getHistory: PropTypes.func.isRequired,
  saveHistoryData: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, {
  getHistory,
  saveHistoryData,
})(History);
