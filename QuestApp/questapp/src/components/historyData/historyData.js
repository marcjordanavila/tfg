import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import firebase from "../../firebase";
import { Link } from "react-router-dom";
import ClipLoader from "react-spinners/ClipLoader";

import { getHistoryData } from "../../actions/generalActions";
import Button from "../basicComponents/button/button";

import "./historyData.css";

class HistoryData extends Component {
  state = {
    loading: false,
  };

  componentDidMount = () => {
    this.setState({
      loading: true,
    });
    let questionnarieId = this.props.generalReducer.historyData.questionnarieId;
    let token = this.props.userReducer.token;
    this.props
      .getHistoryData(questionnarieId, token)
      .then(() => {
        this.setState({
          loading: false,
        });
      })
      .catch((error) => {
        this.setState({
          loading: false,
        });
        if (error.response.status === 666) {
          firebase.auth().signOut();
          this.props.singOut();
        }
      });
  };

  render() {
    return (
      <div className="generalDiv backgroundColorWhite">
        {this.state.loading && (
          <div className="externalSpinnerDiv">
            <div className="spinner">
              <ClipLoader
                className="spinner"
                size={50}
                color={"#00a0b9"}
                loading={this.state.loading}
              />
            </div>
          </div>
        )}
        <div className="col-s-12 col-12 centerText">
          <h1 className="historyDataScreenTitle">
            {this.props.generalReducer.historyData.questionnarieTitle}
          </h1>
        </div>
        {this.props.generalReducer.selectedHistoryQuestionnarie.questionsData &&
          this.props.generalReducer.selectedHistoryQuestionnarie.questionsData.map(
            (question, key) => {
              return (
                <div className="col-s-12 col-12 historyDataTextForm">
                  <hr className="col-10" />
                  <br />
                  <div>{`Pregunta: ${question.Question}`}</div>
                  <br />
                  {this.props.generalReducer.selectedHistoryQuestionnarie
                    .responsesData &&
                    this.props.generalReducer.selectedHistoryQuestionnarie.responsesData.map(
                      (response, key) => {
                        if (question.QuestionId === response.QuestionId) {
                          return (
                            <div>
                              <div>
                                <p>{`${response.Response} --> ${
                                  "Vots: " + response.counter
                                }`}</p>
                                <br />
                              </div>
                            </div>
                          );
                        }
                      }
                    )}
                </div>
              );
            }
          )}
        <div className="col-s-12 col-12 cancelButtonHistory">
          <Link to="principalpanel">
            <Button buttonName="Menú principal" class="buttonStyle" />
          </Link>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    generalReducer: state.generalReducer,
    userReducer: state.userReducer,
  };
};

HistoryData.propTypes = {
  generalReducer: PropTypes.object.isRequired,
  userReducer: PropTypes.object.isRequired,
  getHistoryData: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, { getHistoryData })(HistoryData);
