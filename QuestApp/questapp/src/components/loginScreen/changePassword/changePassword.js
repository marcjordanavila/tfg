import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import firebase from "../../../firebase";

import InputType from "../../basicComponents/inputType/inputType";
import Button from "../../basicComponents/button/button";
import Text from "../../text/text";
import { formValidation } from "../../../validation/validation";

import "./changePassword.css";

class ChangePassword extends Component {
  state = {
    emailInput: "",
    emailInputValue: true,
    correctData: true,
    emailSent: false,
  };

  handleOnChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
    this.validateData(e.target.name, e.target.value);
  };

  validateData = (name, value) => {
    if (!formValidation(name, value)) {
      this.setState({
        [name + "Value"]: false,
      });
    } else {
      this.setState({
        [name + "Value"]: true,
      });
    }
  };

  handleOnClickConfirmButton = () => {
    // Per evitar que ens entrin dades buides
    this.validateData(this.state.emailInput);
    var that = this;
    //Si tot esta bé fem la petició al server
    if (this.state.emailInputValue) {
      this.setState({
        correctData: true,
      });
      firebase
        .auth()
        .sendPasswordResetEmail(this.state.emailInput)
        .then(function () {
          console.log("Correu enviat");
          that.setState({
            emailSent: true,
          });
        })
        .catch(function (error) {
          console.error(error);
        });
    } else {
      this.setState({
        correctData: false,
      });
    }
  };

  handleOnClickCancelButton = () => {
    window.location = "login";
  };

  render() {
    return (
      <div className="changePasswordGeneralDiv backgroundColorWhite">
        <div className="col-s-12 col-12">
          <Text
            text="Correu enviat, revisa la teva bústia"
            class="textColorRed textPasswordNotEqual"
            hidden={!this.state.emailSent}
          />
        </div>
        <div className="col-s-12 col-12">
          <InputType
            title="Correu electrònic"
            name="emailInput"
            type="text"
            onChangeFunction={this.handleOnChange}
            class={this.state.emailInputValue ? "" : "incorrect"}
          />
        </div>
        <div className="col-s-12 col-12">
          <Button
            buttonName="Confirmar"
            onClickButton={this.handleOnClickConfirmButton}
            class="buttonStyle"
          />
        </div>
        <div className="col-s-12 col-12">
          <Link to="/login">
            <Button buttonName="Cancel" class="buttonStyle" />
          </Link>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    userReducer: state.userReducer,
  };
};

ChangePassword.propTypes = {
  userReducer: PropTypes.object.isRequired,
};

export default connect(mapStateToProps, null)(ChangePassword);
