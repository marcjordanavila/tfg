import React, { Component } from "react";
import firebase from "../../../firebase";
import { Link } from "react-router-dom";

import Button from "../../basicComponents/button/button";

export default class Convidat extends Component {
  handleOnClickGuestButton = () => {
    firebase
      .auth()
      .signInAnonymously()
      .catch(function (error) {
        console.error(error.code);
        console.error(error.message);
      });
  };

  render() {
    return (
      <div className="col-s-12 col-12">
        <Link to={"participate"}>
          <Button
            buttonName="Entrar com a convidat"
            onClickButton={this.handleOnClickGuestButton}
            class="buttonStyle"
          />
        </Link>
      </div>
    );
  }
}
