import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import firebase from "../../../firebase";
import ClipLoader from "react-spinners/ClipLoader";

import InputType from "../../basicComponents/inputType/inputType";
import Button from "../../basicComponents/button/button";
import Text from "../../text/text";
import { loginUser } from "../../../actions/userActions";

import "./login.css";

class Login extends Component {
  state = {
    userEmailInput: "",
    passwordInput: "",
    errorMsg: "",
    loading: false,
  };

  handleOnChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  handleOnClickLoginButton = () => {
    this.setState({
      loading: true,
    });

    firebase
      .auth()
      .signInWithEmailAndPassword(
        this.state.userEmailInput,
        this.state.passwordInput
      )
      .then((user) => {
        this.props.loginUser();
        this.setState({
          loading: false,
        });
      })
      .catch((error) => {
        this.setState({ errorMsg: error, loading: false });
      });
  };

  render() {
    return (
      <div className="loginGeneralDiv backgroundColorWhite">
        {this.state.loading && (
          <div className="externalSpinnerDiv">
            <div className="spinner">
              <ClipLoader
                className="spinner"
                size={50}
                color={"#00a0b9"}
                loading={this.state.loading}
              />
            </div>
          </div>
        )}

        <form>
          <div className="col-s-12 col-12">
            <Text
              text={"La contrasenya o l'usuari no son correctes"}
              class="textUserNotExist textBold textColorRed"
              hidden={this.state.errorMsg === ""}
            />
            <InputType
              title="Email"
              name="userEmailInput"
              type="text"
              onChangeFunction={this.handleOnChange}
            />
          </div>
          <div className="col-s-12 col-12">
            <InputType
              title="Contrasenya"
              name="passwordInput"
              type="password"
              onChangeFunction={this.handleOnChange}
            />
          </div>
          <div className="col-s-12 col-12">
            <Button
              buttonName="Iniciar Sessió"
              onClickButton={this.handleOnClickLoginButton}
              class="buttonStyle"
            />
          </div>
          <div className="col-s-12 col-12 centerText marginTop">
            <Link to="/changepassword">Has oblidat al password?</Link>
          </div>
        </form>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    userReducer: state.userReducer,
  };
};

Login.propTypes = {
  userReducer: PropTypes.object.isRequired,
  loginUser: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, {
  loginUser,
})(Login);
