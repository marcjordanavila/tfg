import React, { Component } from "react";

import TabBar from "../tabBar/tabBar";

export default class LoginScreen extends Component {
  render() {
    return (
      <div className="col-s-12 col-12">
        <TabBar
          titleOne={"Iniciar sessió"}
          titleTwo={"Registrar-se"}
          titleThree={"Usuari convidat"}
        />
      </div>
    );
  }
}
