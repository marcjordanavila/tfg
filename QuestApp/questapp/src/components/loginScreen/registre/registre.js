import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import firebase from "../../../firebase";

import InputType from "../../basicComponents/inputType/inputType";
import Button from "../../basicComponents/button/button";
import Text from "../../text/text";
import { formValidation } from "../../../validation/validation";
import { registerUser } from "../../../actions/userActions";

import "./registre.css";

class Registre extends Component {
  state = {
    emailInput: "",
    passwordInput: "",
    passwordTwoInput: "",
    emailInputValue: true,
    passwordInputValue: true,
    passwordTwoInputValue: true,
    hideError: true,
    emptyParams: false,
  };

  handleOnChange = (e) => {
    this.setState(
      {
        [e.target.name]: e.target.value,
      },
      this.validateData(e.target.name, e.target.value)
    );
  };

  emptyParams = () => {
    if (
      this.state.emailInput === "" ||
      this.state.passwordInput === "" ||
      this.state.passwordTwoInput === ""
    ) {
      this.setState({
        emptyParams: true,
      });
      return false;
    } else {
      return true;
    }
  };

  handleOnClickSingUpButton = () => {
    if (
      this.state.emailInputValue &&
      this.state.passwordInputValue &&
      this.state.passwordTwoInputValue &&
      this.state.passwordInput === this.state.passwordTwoInput &&
      this.emptyParams()
    ) {
      firebase
        .auth()
        .createUserWithEmailAndPassword(
          this.state.emailInput,
          this.state.passwordInput
        )
        .then((user) => {
          console.log(user);
        })
        .then(() => {
          firebase.auth().currentUser.sendEmailVerification();
        })
        .catch((error) => {
          this.setState({ error: error });
        });
    } else {
      this.setState({
        hideError: false,
      });
    }
  };

  validateData = (name, value) => {
    if (!formValidation(name, value)) {
      this.setState({
        [name + "Value"]: false,
      });
    } else {
      this.setState({
        hideError: true,
        emptyParams: false,
        [name + "Value"]: true,
      });
    }
  };

  getErrorMessage = () => {
    if (!this.state.emailInputValue) {
      return "El correu electrònic no és correcte";
    }
    if (!this.state.passwordInputValue) {
      return "El password no és correcte, com a mínim ha de tenir 1 minúscula, 1 majúscula i 1 número, una longitud de 8 a 15 caracters i no pot tenir altres simbols";
    }
    if (this.state.passwordInput !== this.state.passwordTwoInput) {
      return "Els passwords son diferents";
    }
    if (this.state.emptyParams) {
      return "Tots els camps son obligatoris";
    }
  };

  render() {
    return (
      <div className="registreGeneralDiv backgroundColorWhite">
        <form>
          <div className="col-s-12 col-12">
            <Text
              text={this.getErrorMessage()}
              class="textUserNotExist textBold textColorRed"
              hidden={this.state.hideError}
            />
          </div>
          <div className="col-s-12 col-12">
            <InputType
              title="Correu electrònic"
              name="emailInput"
              type="email"
              onChangeFunction={this.handleOnChange}
              class={this.state.emailInputValue ? "" : "incorrect"}
            />
          </div>
          <div className="col-s-12 col-12">
            <InputType
              title="Contrasenya"
              name="passwordInput"
              type="password"
              icon="info"
              information="La contrasenya ha de tenir com a mínim 1 minúscula, 1 majúscula i 1 número, una longitud de 8 a 15 caracters i no pot tenir altres simbols"
              onChangeFunction={this.handleOnChange}
              class={this.state.passwordInputValue ? "" : "incorrect"}
            />
          </div>
          <div className="col-s-12 col-12">
            <InputType
              title="Repeteix la contrasenya"
              name="passwordTwoInput"
              type="password"
              onChangeFunction={this.handleOnChange}
              class={this.state.passwordTwoInputValue ? "" : "incorrect"}
            />
          </div>
          <div className="col-s-12 col-12">
            <Button
              buttonName="Registrar-se"
              onClickButton={this.handleOnClickSingUpButton}
              class="buttonStyle"
            />
          </div>
        </form>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    userReducer: state.userReducer,
  };
};

Registre.propTypes = {
  userReducer: PropTypes.object.isRequired,
  registerUser: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, {
  registerUser,
})(Registre);
