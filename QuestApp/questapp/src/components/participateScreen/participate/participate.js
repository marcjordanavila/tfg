import React, { Component } from "react";
import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import firebase from "../../../firebase";
import ClipLoader from "react-spinners/ClipLoader";

import InputType from "../../basicComponents/inputType/inputType";
import Button from "../../basicComponents/button/button";
import Text from "../../text/text";
import {
  participate,
  getParticipateQuestionnarieId,
} from "../../../actions/generalActions";
import { singOut } from "../../../actions/userActions";

import "./participate.css";

class Participate extends Component {
  state = {
    code: "",
    errorCodeMsg: false,
    name: "",
    errorNameMsg: false,
    questionariFounded: false,
    loading: false,
  };

  handleOnChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  handleOnClickParticipateButton = () => {
    this.setState({
      loading: true,
    });
    let participateData = {
      code: this.state.code,
      name: this.state.name,
    };
    this.props.getParticipateQuestionnarieId(this.state.code);
    this.props
      .participate(participateData, this.props.userReducer.token)
      .then((res) => {
        this.setState({
          questionariFounded: true,
          loading: false,
        });
      })
      .catch((error) => {
        if (error.response) {
          if (error.response.status === 601) {
            this.setState({
              errorCodeMsg: true,
              loading: false,
            });
          } else if (error.response.status === 602) {
            this.setState({
              errorNameMsg: true,
              loading: false,
            });
          } else if (error.response.status === 666) {
            firebase.auth().signOut();
            this.props.singOut();
          }
        } else {
          console.error(error.response);
          this.setState({
            loading: false,
            errorCodeMsg: true,
          });
        }
      });
  };

  handelOnClickCancelButton = () => {};

  render() {
    return (
      <div className="participateGeneralDiv backgroundColorWhite">
        {this.state.loading && (
          <div className="externalSpinnerDiv">
            <div className="spinner">
              <ClipLoader
                className="spinner"
                size={50}
                color={"#00a0b9"}
                loading={this.state.loading}
              />
            </div>
          </div>
        )}
        {this.state.questionariFounded && <Redirect to="userparticipating" />}
        <form>
          <div className="col-s-12 col-12">
            <Text
              text={"El codi no és correcte o el qüestionari està tancat"}
              class="questNotExist textBold textColorRed centerText"
              hidden={!this.state.errorCodeMsg}
            />
            <InputType
              title="Codi qüestionari"
              name="code"
              type="text"
              onChangeFunction={this.handleOnChange}
            />
          </div>
          <div className="col-s-12 col-12">
            <Text
              text={"El nom de participant ja esta en ús"}
              class="questNotExist textBold textColorRed"
              hidden={!this.state.errorNameMsg}
            />
            <InputType
              title="Nom participant"
              name="name"
              type="text"
              onChangeFunction={this.handleOnChange}
            />
          </div>
          <div className="col-s-12 col-12">
            <Button
              buttonName="Participar"
              onClickButton={this.handleOnClickParticipateButton}
              class="buttonStyle"
            />
          </div>
          <div className="col-s-12 col-12">
            <Link to="principalPanel">
              <Button
                buttonName="Cancelar"
                onClickButton={this.handelOnClickCancelButton}
                class="buttonStyle"
              />
            </Link>
          </div>
        </form>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    generalReducer: state.generalReducer,
    userReducer: state.userReducer,
  };
};

Participate.propTypes = {
  generalReducer: PropTypes.object.isRequired,
  userReducer: PropTypes.object.isRequired,
  participate: PropTypes.func.isRequired,
  singOut: PropTypes.func.isRequired,
  getParticipateQuestionnarieId: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, {
  participate,
  singOut,
  getParticipateQuestionnarieId,
})(Participate);
