import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import firebase from "../../../../firebase";
import ClipLoader from "react-spinners/ClipLoader";

import Button from "../../../basicComponents/button/button";
import {
  getQuestionsAndResponsesSelectedQuestionnarie,
  setQuestionariParticipatingOff,
  setQuestionariToHistory,
} from "../../../../actions/generalActions";
import { singOut } from "../../../../actions/userActions";

import "./adminParticipating.css";

class AdminParticipating extends Component {
  constructor() {
    super();
    this.unsubscribeArray = [];
  }

  state = {
    arrayDataCounter: [],
    loading: false,
  };

  componentDidMount = () => {
    this.setState({
      loading: true,
    });
    let questionnarieId = this.props.generalReducer.selectedQuestionnarie
      .QuestionnarieId;
    let token = this.props.userReducer.token;
    let that = this;
    //let dataObject = {};
    let dataArray = [];
    this.props
      .getQuestionsAndResponsesSelectedQuestionnarie(questionnarieId, token)
      .then((res) => {
        this.setState({
          loading: false,
        });
        res.payload.responsesData.map((response, key) => {
          // Posem l'id de la resposta en el array
          dataArray.push({ responseId: response.ResponseId });
          that.unsubscribe = firebase
            .firestore()
            .collection("ResponsesParticipants")
            .where("ResponseId", "==", response.ResponseId)
            .onSnapshot(function (querySnapshot) {
              let counter = 0;
              let index = 0;
              // Sumem el numero de respostes que te cada resposta
              querySnapshot.forEach(function (doc) {
                counter++;
              });
              // Coloquem el contador en la posició del array que toca
              for (index in dataArray) {
                if (dataArray[index].responseId === response.ResponseId) {
                  dataArray[index]["counter"] = counter;
                }
              }
              // Pasem el resultat a l'estat
              that.setState({
                arrayDataCounter: dataArray,
              });
              return null;
            });
          that.unsubscribeArray.push(that.unsubscribe);
        });
      })
      .catch((error) => {
        this.setState({
          loading: false,
        });
        if (error.response && error.response.status === 666) {
          firebase.auth().signOut();
          this.props.singOut();
        }
      });
  };

  handelOnClickEndButton = () => {
    for (let i = 0; i < this.unsubscribeArray.length; i++) {
      this.unsubscribeArray[i]();
    }
    let token = this.props.userReducer.token;
    let questionnarieId = this.props.generalReducer.selectedQuestionnarie
      .QuestionnarieId;
    this.props.setQuestionariParticipatingOff(questionnarieId, token);
    this.props.setQuestionariToHistory(questionnarieId, token);
  };

  getResponseNumber = (responseId) => {
    let counter = 0;
    this.state.arrayDataCounter.map((response) => {
      if (response.responseId === responseId) {
        counter = response.counter;
      }
    });
    return counter;
  };

  render() {
    let that = this;
    return (
      <div className="generalDiv backgroundColorWhite">
        {this.state.loading && (
          <div className="externalSpinnerDiv">
            <div className="spinner">
              <ClipLoader
                className="spinner"
                size={50}
                color={"#00a0b9"}
                loading={this.state.loading}
              />
            </div>
          </div>
        )}
        <div className="col-s-12 col-12 centerText">
          <h1 className="questionnarieDataScreenTitle">
            {this.props.generalReducer.selectedQuestionnarie.QuestionnarieName}
          </h1>
        </div>
        <form>
          {this.props.generalReducer.questionnarieObserver.questionsData &&
            this.props.generalReducer.questionnarieObserver.questionsData.map(
              (question, key) => {
                let counter = 1;
                return (
                  <div
                    className="col-s-12 col-12 adminParticipatingTextForm"
                    key={key}
                  >
                    <hr className="col-10" />
                    <br />
                    <h3>
                      Pregunta {key + 1}: {question.Question}{" "}
                    </h3>
                    <br />
                    {this.props.generalReducer.questionnarieObserver.responsesData.map(
                      (response, key) => {
                        if (question.QuestionId === response.QuestionId) {
                          return (
                            <div key={key}>
                              <p>
                                {`${
                                  response.Response
                                } --> Vots: ${this.getResponseNumber(
                                  response.ResponseId
                                )}`}{" "}
                              </p>
                              <br />
                            </div>
                          );
                        }
                      }
                    )}
                  </div>
                );
              }
            )}

          <div className="col-s-12 col-12">
            <Link to="principalPanel">
              <Button
                buttonName="Finalitzar"
                onClickButton={this.handelOnClickEndButton}
                class="buttonStyle"
              />
            </Link>
          </div>
        </form>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    generalReducer: state.generalReducer,
    userReducer: state.userReducer,
  };
};

AdminParticipating.propTypes = {
  generalReducer: PropTypes.object.isRequired,
  userReducer: PropTypes.object.isRequired,
  getQuestionsAndResponsesSelectedQuestionnarie: PropTypes.func.isRequired,
  singOut: PropTypes.func.isRequired,
  setQuestionariToHistory: PropTypes.func.isRequired,
  setQuestionariParticipatingOff: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, {
  getQuestionsAndResponsesSelectedQuestionnarie,
  singOut,
  setQuestionariToHistory,
  setQuestionariParticipatingOff,
})(AdminParticipating);
