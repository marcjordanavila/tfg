import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import firebase from "../../../../firebase";

import {
  getQuestionsAndResponsesSelectedQuestionnarie,
  saveResponse,
} from "../../../../actions/generalActions";
import Button from "../../../basicComponents/button/button";

class UserParticipating extends Component {
  constructor() {
    super();
    this.unsuscribe = "";
  }

  state = {
    active: 0,
    question: 1,
  };

  componentDidMount = () => {
    let that = this;
    let questionnarieId = this.props.generalReducer.questionnariePartipatingId;
    let token = this.props.userReducer.token;
    this.unsuscribe = firebase
      .firestore()
      .collection("Questionnaries")
      .doc(questionnarieId)
      .onSnapshot(function (doc) {
        if (doc.data().Participating === 1) {
          that.setState({
            active: 1,
          });
        }
      });
    this.props.getQuestionsAndResponsesSelectedQuestionnarie(
      questionnarieId,
      token
    );
  };

  sendResponse = (responseId, participantId, token) => {
    this.setState({ question: this.state.question + 1 });
    let responseData = {
      responseId: responseId,
      participantId: participantId,
    };
    this.props.saveResponse(responseData, token);
  };

  getScreenData = () => {
    console.log("getScreenData");
    let numberOfQuestions = this.props.generalReducer.questionnarieObserver
      .questionsData.length;
    let participantId = this.props.generalReducer.participantId;
    let token = this.props.userReducer.token;
    if (this.state.question <= numberOfQuestions) {
      return (
        <div>
          <p>Pregunta {this.state.question}</p>
          <p>
            {
              this.props.generalReducer.questionnarieObserver.questionsData[
                this.state.question - 1
              ].Question
            }
          </p>
          {this.props.generalReducer.questionnarieObserver.responsesData.map(
            (response, key) => {
              if (
                this.props.generalReducer.questionnarieObserver.questionsData[
                  this.state.question - 1
                ].QuestionId === response.QuestionId
              ) {
                return (
                  <div key={key}>
                    <Button
                      buttonName={response.Response}
                      class="buttonStyle"
                      onClickButton={() =>
                        this.sendResponse(
                          response.ResponseId,
                          participantId,
                          token
                        )
                      }
                    />
                  </div>
                );
              }
            }
          )}
        </div>
      );
    } else {
      return (
        <div>
          <div className="col-s-12 col-12">
            <p>Qüestionari completat</p>
          </div>
          <div className="col-s-12 col-12 cancelButtonQuestionnaries">
            <Link to="principalpanel">
              <Button buttonName="Finalitzar" class="buttonStyle" />
            </Link>
          </div>
        </div>
      );
    }
  };

  render() {
    return (
      <div className="generalDiv backgroundColorWhite">
        {this.state.active === 0 ? (
          <div className="centerText">
            <p>Esperant a que comenci el qüestionari</p>
          </div>
        ) : (
          this.getScreenData()
        )}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    generalReducer: state.generalReducer,
    userReducer: state.userReducer,
  };
};

UserParticipating.propTypes = {
  generalReducer: PropTypes.object.isRequired,
  userReducer: PropTypes.object.isRequired,
  getQuestionsAndResponsesSelectedQuestionnarie: PropTypes.func.isRequired,
  saveResponse: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, {
  getQuestionsAndResponsesSelectedQuestionnarie,
  saveResponse,
})(UserParticipating);
