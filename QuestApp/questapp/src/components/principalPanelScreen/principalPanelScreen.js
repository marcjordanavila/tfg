import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import firebase from "firebase";

import Button from "../basicComponents/button/button";
import { singOut } from "../../actions/userActions";

import "./principalPanelScreen.css";

class PrincipalPanelScreen extends Component {
  handleOnClickNewQuestionari = () => {};

  handleOnClickParticipate = () => {};

  handleOnClickQuestionari = () => {};

  handleOnClickHistorial = () => {};

  handleOnClickTancarSessio = () => {
    firebase.auth().signOut();
    this.props.singOut();
  };

  render() {
    return (
      <div>
        <div className="principalPanelGeneralDiv backgroundColorWhite">
          <div className="col-s-12 col-12">
            {this.props.generalReducer.showMessageFormCreated && (
              <p className="centerText">Questionari creat amb exit </p>
            )}
          </div>
          <div className="col-s-12 col-12">
            <Link to="/createform">
              <Button
                buttonName="Crear Qüestionari"
                onClickButton={this.handleOnClickNewQuestionari}
                class="buttonStyle"
                disabled={this.props.userReducer.isAnonymous}
              />
            </Link>
          </div>
          <div className="col-s-12 col-12">
            <Link to="/participate">
              <Button
                buttonName="Participar"
                onClickButton={this.handleOnClickParticipate}
                class="buttonStyle "
              />
            </Link>
          </div>
          <div className="col-s-12 col-12">
            <Link to="/questionnaires">
              <Button
                buttonName="Qüestionaris"
                onClickButton={this.handleOnClickQuestionari}
                class="buttonStyle questionaryButton"
                disabled={this.props.userReducer.isAnonymous}
              />
            </Link>
          </div>
          <div className="col-s-12 col-12">
            <Link to="/history">
              <Button
                buttonName="Historial"
                onClickButton={this.handleOnClickHistorial}
                class="buttonStyle"
                disabled={this.props.userReducer.isAnonymous}
              />
            </Link>
          </div>
          <div className="col-s-12 col-12">
            <Button
              buttonName="Tancar sessió"
              onClickButton={this.handleOnClickTancarSessio}
              class="logoutButton"
            />
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    userReducer: state.userReducer,
    generalReducer: state.generalReducer,
  };
};

PrincipalPanelScreen.propTypes = {
  userReducer: PropTypes.object.isRequired,
  generalReducer: PropTypes.object.isRequired,
  singOut: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, { singOut })(PrincipalPanelScreen);
