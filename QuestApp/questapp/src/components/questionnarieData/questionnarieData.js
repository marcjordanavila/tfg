import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import firebase from "../../firebase";

import {
  deleteQuestionnarie,
  setQuestionariParticipateOff,
  setQuestionariParticipateOn,
  setQuestionariParticipatingOn,
} from "../../actions/generalActions";
import { singOut } from "../../actions/userActions";
import Button from "../basicComponents/button/button";

import "./questionnarieData.css";

class QuestionnarieData extends Component {
  constructor() {
    super();
    this.unsuscribe = "";
  }

  state = {
    inscriptionScreen: false,
    participants: [],
  };

  handleOnClickDeleteButton = () => {
    let questionnarieData = {
      questionnarieId: this.props.generalReducer.selectedQuestionnarie
        .QuestionnarieId,
      userId: this.props.userReducer.userId,
    };
    this.props
      .deleteQuestionnarie(questionnarieData, this.props.userReducer.token)
      .catch((error) => {
        if (error.response.status === 666) {
          firebase.auth().signOut();
          this.props.singOut();
        }
      });
  };

  handleOnClickParticipantsInscription = () => {
    let questId = this.props.generalReducer.selectedQuestionnarie
      .QuestionnarieId;
    let token = this.props.userReducer.token;
    let activeParticipants = [];
    let that = this;
    // Posem en el qüestionari participate = 1
    this.props.setQuestionariParticipateOn(questId, token).catch((error) => {
      if (error.response.status === 666) {
        firebase.auth().signOut();
        this.props.singOut();
      }
    });

    // Ens suscribim a la taula participants per veure quants n'hi han
    this.unsuscribe = firebase
      .firestore()
      .collection("Participants")
      .where("QuestionnarieId", "==", questId)
      .onSnapshot(function (querySnapshot) {
        activeParticipants = [];
        querySnapshot.forEach(function (doc) {
          activeParticipants.push(doc.data().ParticipantName);
        });
        that.setState({
          participants: activeParticipants,
        });
      });
    this.setState({
      inscriptionScreen: true,
    });
  };

  handleOnClickStartQuest = () => {
    let questId = this.props.generalReducer.selectedQuestionnarie
      .QuestionnarieId;
    let token = this.props.userReducer.token;
    //Posem en el qüestionari participate = 0
    this.props.setQuestionariParticipateOff(questId, token);
    this.props.setQuestionariParticipatingOn(questId, token);
    this.unsuscribe();
  };

  getScreenRender = () => {
    if (!this.state.inscriptionScreen) {
      return (
        <>
          <div className="col-s-12 col-12 ">
            <Button
              buttonName="Inscripció d'usuaris"
              class="buttonStyle"
              onClickButton={this.handleOnClickParticipantsInscription}
            />
          </div>
          <div className="col-s-12 col-12 cancelButtonQuestionnaries">
            <div className="col-s-12 col-12">
              <Link to="questionnaires">
                <Button
                  buttonName="Eliminar"
                  class="buttonStyle"
                  onClickButton={this.handleOnClickDeleteButton}
                />
              </Link>
            </div>
            <div className="col-s-12 col-12">
              <Link to="questionnaires">
                <Button buttonName="Cancelar" class="buttonStyle" />
              </Link>
            </div>
          </div>
        </>
      );
    } else if (this.state.inscriptionScreen) {
      return (
        <>
          <div className="col-s-12 col-12 centerText">
            <p className="questionnarieDataCode">
              Número de participants: {this.state.participants.length}
            </p>
          </div>
          <div className="col-s-12 col-12 ">
            <Link to="adminparticipating">
              <Button
                buttonName="Començar qüestionari"
                class="buttonStyle"
                onClickButton={this.handleOnClickStartQuest}
              />
            </Link>
          </div>
          <div className="col-s-12 col-12">
            <Link to="questionnaires">
              <Button buttonName="Cancelar" class="buttonStyle" />
            </Link>
          </div>
        </>
      );
    }
  };

  render() {
    return (
      <div className="generalDiv backgroundColorWhite">
        <div className="col-s-12 col-12 centerText">
          <h1 className="questionnarieDataScreenTitle">
            {this.props.generalReducer.selectedQuestionnarie.QuestionnarieName}
          </h1>
        </div>
        <div className="col-s-12 col-12 centerText">
          <p className="questionnarieDataCode">
            Codi del qüestionari{" "}
            {this.props.generalReducer.selectedQuestionnarie.QuestionnarieId}
          </p>
        </div>
        {this.getScreenRender()}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    generalReducer: state.generalReducer,
    userReducer: state.userReducer,
  };
};

QuestionnarieData.propTypes = {
  generalReducer: PropTypes.object.isRequired,
  userReducer: PropTypes.object.isRequired,
  deleteQuestionnarie: PropTypes.func.isRequired,
  singOut: PropTypes.func.isRequired,
  setQuestionariParticipateOff: PropTypes.func.isRequired,
  setQuestionariParticipateOn: PropTypes.func.isRequired,
  setQuestionariParticipatingOn: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, {
  deleteQuestionnarie,
  singOut,
  setQuestionariParticipateOff,
  setQuestionariParticipateOn,
  setQuestionariParticipatingOn,
})(QuestionnarieData);
