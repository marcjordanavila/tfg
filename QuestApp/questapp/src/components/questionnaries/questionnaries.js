import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import firebase from "../../firebase";
import ClipLoader from "react-spinners/ClipLoader";

import {
  getActiveQuestionnaries,
  selectedQuestionnarie,
} from "../../actions/generalActions";
import { singOut } from "../../actions/userActions";
import Button from "../basicComponents/button/button";

import "./questionnaries.css";

class Questionnaries extends Component {
  state = {
    loading: false,
  };

  componentDidMount = () => {
    this.setState({
      loading: true,
    });
    let userId, token;
    ({ userId, token } = this.props.userReducer);
    this.props
      .getActiveQuestionnaries(userId, token)
      .then(() => {
        this.setState({
          loading: false,
        });
      })
      .catch((error) => {
        this.setState({
          loading: false,
        });
        if (error.response.status === 666) {
          firebase.auth().signOut();
          this.props.singOut();
        }
      });
  };

  handleOnClickQuestionnariButton = (key) => {
    this.props.selectedQuestionnarie(
      this.props.generalReducer.activeQuestionnaries[key]
    );
  };

  render() {
    return (
      <div className="generalDiv backgroundColorWhite">
        {this.state.loading && (
          <div className="externalSpinnerDiv">
            <div className="spinner">
              <ClipLoader
                className="spinner"
                size={50}
                color={"#00a0b9"}
                loading={this.state.loading}
              />
            </div>
          </div>
        )}
        <div className="col-s-12 col-12 centerText questionnarieScreenTitle">
          <h1>Qüestionaris actius</h1>
        </div>
        {this.props.generalReducer.activeQuestionnaries.length === 0 ? (
          <div className="col-s-12 col-12 centerText">
            <p>No té cap qüestionari actiu</p>
          </div>
        ) : (
          this.props.generalReducer.activeQuestionnaries &&
          this.props.generalReducer.activeQuestionnaries.map(
            (questionnarie, key) => {
              return (
                <div className="col-s-12 col-12" key={key}>
                  <Link to="questionnairedata">
                    <Button
                      buttonName={questionnarie.QuestionnarieName}
                      onClickButton={() =>
                        this.handleOnClickQuestionnariButton(key)
                      }
                      class="buttonStyle "
                    />
                  </Link>
                </div>
              );
            }
          )
        )}
        <div className="col-s-12 col-12 cancelButtonQuestionnaries">
          <Link to="principalpanel">
            <Button buttonName="Cancelar" class="buttonStyle" />
          </Link>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    generalReducer: state.generalReducer,
    userReducer: state.userReducer,
  };
};

Questionnaries.propTypes = {
  generalReducer: PropTypes.object.isRequired,
  userReducer: PropTypes.object.isRequired,
  getActiveQuestionnaries: PropTypes.func.isRequired,
  selectedQuestionnarie: PropTypes.func.isRequired,
  singOut: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, {
  getActiveQuestionnaries,
  selectedQuestionnarie,
  singOut,
})(Questionnaries);
