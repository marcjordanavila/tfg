import React, { useState } from "react";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";

import Login from "../loginScreen/login/login";
import Registre from "../loginScreen/registre/registre";
import Convidat from "../loginScreen/convidat/convidat";

import "react-tabs/style/react-tabs.css";
import "./tabBar.css";

export default function TabBar(props) {
  const [index, setIndex] = useState(2);

  return (
    <div className="col-s-12 col-12">
      <Tabs defaultIndex={index} className="col-s-12 col-12">
        <TabList className="react-tabs__tab-list col-s-12 col-12">
          <Tab
            className="react-tabs__tab col-s-4 col-4"
            onClick={() => setIndex(0)}
          >
            {props.titleOne}
          </Tab>
          <Tab
            className="react-tabs__tab col-s-4 col-4"
            onClick={() => setIndex(1)}
          >
            {props.titleTwo}
          </Tab>
          <Tab
            className="react-tabs__tab col-s-4 col-4"
            onClick={() => setIndex(2)}
          >
            {props.titleThree}
          </Tab>
        </TabList>

        <TabPanel>
          <Login />
        </TabPanel>
        <TabPanel>
          <Registre />
        </TabPanel>
        <TabPanel>
          <Convidat />
        </TabPanel>
      </Tabs>
    </div>
  );
}
