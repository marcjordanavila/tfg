import React from "react";

import "./text.css";

export default function text(props) {
  return (
    <div>
      <p className={props.class} hidden={props.hidden}>
        {props.text}
      </p>
    </div>
  );
}
