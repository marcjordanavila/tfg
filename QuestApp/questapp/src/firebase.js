import firebase from "firebase";

const config = {
  apiKey: "AIzaSyCez7QSkVqW3LZ-KNhe5hq866w22oLI9fU",
  authDomain: "appquest-dcb91.firebaseapp.com",
  databaseURL: "https://appquest-dcb91.firebaseio.com",
  projectId: "appquest-dcb91",
  storageBucket: "appquest-dcb91.appspot.com",
  messagingSenderId: "742781062182",
  appId: "1:742781062182:web:cdd32feddb36005d27eae7",
  measurementId: "G-6556EL2TFE",
};

firebase.initializeApp(config);
export default firebase;
