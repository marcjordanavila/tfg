//import isEmpty from "../validation/is-empty";

import * as actionTypes from "../actions/actions";

const initialState = {
  activeQuestionnaries: [],
  form: {
    name: "",
    active: 1, // 0 inactive | 1 active
    participate: 0, // 0 inscription off | 1 inscription on
    participating: 0, // 0 waiting | 1 participating
    userId: "",
    key: "",
  },
  historyQuestions: [],
  historyData: {},
  questionnarieObserver: [],
  questionnariePartipatingId: "",
  questionnarieData: [],
  questionButtons: [1],
  questions: [
    {
      name: "Pregunta 1",
      question: "",
      questionNumber: 1,
      responses: [
        {
          name: "Resposta 1",
          counter: 1,
          response: "",
        },
        {
          name: "Resposta 2",
          counter: 2,
          response: "",
        },
      ],
    },
  ],
  participantId: "",
  selectedQuestion: 0,
  selectedQuestionnarie: "",
  selectedHistoryQuestionnarie: {},
  showMessageFormCreated: false,
};

export default function (state = initialState, action) {
  let questionNumber = "";
  switch (action.type) {
    case actionTypes.ADD_NAME_FORM:
      return {
        ...state,
        form: {
          ...state.form,
          name: action.payload,
        },
      };
    case actionTypes.ADD_QUESTION_FORM:
      let nextButton = state.questionButtons.length + 1;
      let counterQuestion =
        state.questions[state.questions.length - 1].questionNumber + 1;
      let nextButtonName = "Pregunta " + (state.questionButtons.length + 1);
      return {
        ...state,
        questionButtons: [...state.questionButtons, nextButton],
        questions: [
          ...state.questions,
          {
            name: nextButtonName,
            question: "",
            questionNumber: counterQuestion,
            responses: [
              {
                name: "Resposta 1",
                counter: 1,
                response: "",
              },
              {
                name: "Resposta 2",
                counter: 2,
                response: "",
              },
            ],
          },
        ],
      };
    case actionTypes.ADD_QUESTION_TEXT:
      let questionToChange = action.payload.question;
      questionNumber = action.payload.questionNumber;
      return {
        ...state,
        questions: state.questions.map((question, i) =>
          i === questionNumber
            ? { ...question, question: questionToChange }
            : question
        ),
      };
    case actionTypes.ADD_RESPONSE_TO_QUESTION:
      questionNumber = action.payload;
      let counterNumber =
        state.questions[questionNumber].responses[
          state.questions[questionNumber].responses.length - 1
        ].counter + 1;
      let addedResponse = {
        name: "Resposta " + counterNumber,
        counter: counterNumber,
        response: "",
      };
      return {
        ...state,
        questions: state.questions.map((question, i) => {
          return i === questionNumber
            ? {
                ...question,
                responses: [...question.responses, addedResponse],
              }
            : question;
        }),
      };
    case actionTypes.ADD_RESPONSE_TEXT_TO_QUESTION:
      questionNumber = action.payload.questionNumber;
      let auxArray = action.payload.responses;
      return {
        ...state,
        questions: state.questions.map((question, i) => {
          return i === questionNumber
            ? {
                ...question,
                responses: question.responses.map((content, j) => {
                  if (true) {
                    return {
                      ...content,
                      response: auxArray[j],
                    };
                  }
                }),
              }
            : question;
        }),
      };

    case actionTypes.CANCEL_QUESTION_AND_RESPONSE:
      return {
        ...state,
      };
    case actionTypes.CANCEL_QUESTION_FORM:
      return {
        ...state,
        showMessageFormCreated: false,
        questionButtons: [1],
        form: {
          name: "",
          active: 1, // 0 inactive | 1 active
          participate: 0, // 0 inscription off | 1 inscription on
          participating: 0, // 0 waiting | 1 participating
          userId: "",
          key: "",
        },
        questions: [
          {
            name: "Pregunta 1",
            question: "",
            questionNumber: 1,
            responses: [
              {
                name: "Resposta 1",
                counter: 1,
                response: "",
              },
              {
                name: "Resposta 2",
                counter: 2,
                response: "",
              },
            ],
          },
        ],
      };
    case actionTypes.CREATE_FORM:
      return {
        ...state,
        form: {
          name: "",
          active: 1, // 0 inactive | 1 active
          participate: 0, // 0 inscription off | 1 inscription on
          participating: 0, // 0 waiting | 1 participating
          userId: "",
          key: "",
        },
        showMessageFormCreated: true,
        questionButtons: [1],
        selectedQuestion: 0,
        questions: [
          {
            name: "Pregunta 1",
            question: "",
            questionNumber: 1,
            responses: [
              {
                name: "Resposta 1",
                counter: 1,
                response: "",
              },
              {
                name: "Resposta 2",
                counter: 2,
                response: "",
              },
            ],
          },
        ],
      };
    case actionTypes.DELETE_QUESTION_AND_RESPONSES:
      questionNumber = action.payload;
      return {
        ...state,
        questions: state.questions.filter(
          (item, index) => index !== questionNumber
        ),
        questionButtons: state.questionButtons.filter(
          (item, index) => index !== questionNumber
        ),
      };
    case actionTypes.DELETE_QUESTIONNARIE:
      let questionnarieId = action.payload;
      return {
        ...state,
        activeQuestionnaries: state.activeQuestionnaries.filter(
          (item) => item.QuestionnarieId !== questionnarieId
        ),
      };
    case actionTypes.DELETE_RESPONSE:
      questionNumber = action.payload.questionNumber;
      let responseNumber = action.payload.index;
      return {
        ...state,
        questions: state.questions.map((question, i) => {
          return i === questionNumber
            ? {
                ...question,
                responses: question.responses.filter(
                  (item, index) => index !== responseNumber
                ),
              }
            : question;
        }),
      };
    case actionTypes.GET_ACTIVE_QUESTIONARIES:
      return {
        ...state,
        activeQuestionnaries: action.payload,
      };
    case actionTypes.GET_HISTORY:
      return {
        ...state,
        historyQuestions: action.payload,
      };
    case actionTypes.GET_HISTORY_DATA:
      let questionsDataHistory = action.payload.questionsData;
      let responsesDataHistory = action.payload.responsesData;
      questionsDataHistory.sort((a, b) =>
        a.Order > b.Order ? 1 : b.Order > a.Order ? -1 : 0
      );
      responsesDataHistory.sort((a, b) =>
        a.Order > b.Order ? 1 : b.Order > a.Order ? -1 : 0
      );
      let dataHistory = {
        questionsData: questionsDataHistory,
        responsesData: responsesDataHistory,
      };
      return {
        ...state,
        selectedHistoryQuestionnarie: dataHistory,
      };
    case actionTypes.GET_QUESTIONNARIE_DATA:
      return {
        ...state,
        questionnarieData: action.payload,
      };
    case actionTypes.GET_QUESTIONS_AND_RESPOSNES_SELECTED_QUESTIONARIE:
      // Ordenem els resultats segons el camp Order
      let questionsData = action.payload.questionsData;
      let responsesData = action.payload.responsesData;
      questionsData.sort((a, b) =>
        a.Order > b.Order ? 1 : b.Order > a.Order ? -1 : 0
      );
      responsesData.sort((a, b) =>
        a.Order > b.Order ? 1 : b.Order > a.Order ? -1 : 0
      );
      let data = { questionsData: questionsData, responsesData: responsesData };
      return {
        ...state,
        questionnarieObserver: data,
      };
    case actionTypes.GET_PARTICIPATE_QUESTIONNARIE_ID:
      return {
        ...state,
        questionnariePartipatingId: action.payload,
      };
    case actionTypes.PARTICIPATE:
      return {
        ...state,
        participantId: action.payload.participantId,
      };
    case actionTypes.SAVE_HISTORY_DATA:
      return {
        ...state,
        historyData: action.payload,
      };
    case actionTypes.SAVE_RESPONSE:
      return {
        ...state,
      };
    case actionTypes.SELECTED_QUESTION:
      return {
        ...state,
        selectedQuestion: action.payload,
      };
    case actionTypes.SELECTED_QUESTIONARI:
      return {
        ...state,
        selectedQuestionnarie: action.payload,
      };
    case actionTypes.SET_PARTICIPATE_ON:
      return {
        ...state,
      };
    case actionTypes.SET_PARTICIPATE_OFF:
      return {
        ...state,
      };
    case actionTypes.SET_PARTICIPATING_ON:
      return {
        ...state,
      };
    case actionTypes.SET_PARTICIPATING_OFF:
      return {
        ...state,
      };
    case actionTypes.SET_TO_HISTORY:
      return {
        ...state,
      };
    case actionTypes.SING_OUT:
      return {
        activeQuestionnaries: [],
        form: {
          name: "",
          active: 1, // 0 inactive | 1 active
          participate: 0, // 0 inscription off | 1 inscription on
          participating: 0, // 0 waiting | 1 participating
          userId: "",
          key: "",
        },
        historyQuestions: [],
        questionnarieObserver: [],
        questionnariePartipatingId: "",
        questionnarieData: [],
        questionButtons: [1],
        questions: [
          {
            name: "Pregunta 1",
            question: "",
            questionNumber: 1,
            responses: [
              {
                name: "Resposta 1",
                counter: 1,
                response: "",
              },
              {
                name: "Resposta 2",
                counter: 2,
                response: "",
              },
            ],
          },
        ],
        participantId: "",
        selectedQuestion: 0,
        selectedQuestionnarie: "",
        selectedHistoryQuestionnarie: {},
        showMessageFormCreated: false,
      };
    default:
      return state;
  }
}
