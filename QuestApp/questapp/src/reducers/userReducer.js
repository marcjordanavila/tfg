//import isEmpty from "../validation/is-empty";
import * as actionTypes from "../actions/actions";

const initialState = {
  userAuth: "",
  isAnonymous: false,
  token: "",
  userId: "",
};
//localStorage.setItem("token", action.payload.userInfo.token);
export default function (state = initialState, action) {
  switch (action.type) {
    case actionTypes.GET_TOKEN:
      return {
        ...state,
        token: action.payload,
      };
    case actionTypes.GET_USER_ID:
      return {
        ...state,
        userId: action.payload,
      };
    case actionTypes.SING_UP_USER:
      return {
        ...state,
        userAuth: true,
      };
    case actionTypes.SING_IN_USER:
      return {
        ...state,
        userAuth: true,
      };
    case actionTypes.IS_LOGGED:
      return {
        ...state,
        userAuth: true,
      };
    case actionTypes.IS_NOT_LOGGED:
      return {
        ...state,
        userAuth: false,
      };
    case actionTypes.SING_OUT:
      return {
        ...state,
        userAuth: "",
        isAnonymous: false,
        token: "",
      };
    case actionTypes.IS_ANONIMOUS:
      console.log(action.payload);
      return {
        isAnonymous: action.payload,
      };
    default:
      return state;
  }
}
