export const formValidation = (field, value) => {
  //expresions regulars
  const nameValidation = /^[a-zA-Z ]{3,25}$/;
  const passwordValidation = /^(?=\w*\d)(?=\w*[A-Z])(?=\w*[a-z])\S{8,15}$/;
  const emailValidation = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.([a-zA-Z]{2,4})$/;
  switch (field) {
    case "userNameInput":
      if (nameValidation.test(value)) return true;
      break;
    case "passwordInput":
      if (passwordValidation.test(value)) return true;
      break;
    case "passwordTwoInput":
      if (passwordValidation.test(value)) return true;
      break;
    case "newPasswordOneInput":
      if (passwordValidation.test(value)) return true;
      break;
    case "newPasswordTwoInput":
      if (passwordValidation.test(value)) return true;
      break;
    case "emailInput":
      if (emailValidation.test(value)) return true;
      break;
    default:
      break;
  }
  return false;
};
